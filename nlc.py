from __future__ import print_function
import json
import os

# from os.path import join, dirname
from watson_developer_cloud import NaturalLanguageClassifierV1

natural_language_classifier = NaturalLanguageClassifierV1(
    username='01e15620-639b-4f42-866e-cc8a415b5032',
    password='ZQz1HeQIAPV4')

with open('./train.csv', 'rb') as training_data:
  classifier = natural_language_classifier.create_classifier(
    training_data=training_data,
    # before Python SDK v1.0.0, name and language were top-level parameters
    metadata='{"name": "Standup","language": "en"}
  )
print(json.dumps(classifier, indent=2))
classifiers = natural_language_classifier.list_classifiers()
print(json.dumps(classifiers, indent=2))
status = natural_language_classifier.get_classifier('e9fa7bx367-nlc-596')
print(status['status'])
print (json.dumps(status, indent=2))
classes = natural_language_classifier.classify('e9fa7bx367-nlc-596', 'How hot will it be today?')
print(json.dumps(classes, indent=2))