from __future__ import print_function
import json
import os

# from os.path import join, dirname
from watson_developer_cloud import NaturalLanguageClassifierV1

natural_language_classifier = NaturalLanguageClassifierV1(
    username='8d12b321-aab5-42ee-8798-2ba30fcdc179',
    password='Oy2oc26bkdUy')


# create a classifier
with open(os.path.join(os.path.dirname(__file__), 'test.csv'), 'rb') as training_qdata:
    metadata = json.dumps({'name': 'my-classifier', 'language': 'en'})
    classifier = natural_language_classifier.create_classifier(
        metadata=metadata,
        training_data=training_qdata
    )
    classifier_id = classifier['classifier_id']
    print(json.dumps(classifier, indent=2))

status = natural_language_classifier.get_classifier(classifier_id)
print(json.dumps(status, indent=2))
